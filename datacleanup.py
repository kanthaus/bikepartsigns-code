#!/usr/bin/env python3
"""Collection of function to clean up data.

Also used for storing code that might be useful
for cleaning data but is not in use right now."""

import json
import argparse
import os
import subprocess
import logging
import imagesize

from generate_labels import get_parts, check_filename_types
from generate_labels import add_file_args

LOGGER = logging.getLogger(__name__)


def parse_args():
    """Parse the command-line arguments.

    Supply information from command-line arguments.

    Add various subparsers that have each a func
    default that tells the main program which
    function should be called if this subcommand
    is issued.

    Returns:
        Options as an dict-like object.

    """
    parser = argparse.ArgumentParser(
        description="""Collection of various data cleaning tasks."""
    )
    add_file_args(parser)
    parser.add_argument('--outputfile', '-o',
                        default='parts-sorted.json',
                        help=('File where to save the '
                              + 'sorted parts list.'
                              + 'Relative to out directory.'),
                        type=check_filename_types(['.json'])
                        )

    subparsersgroup = parser.add_subparsers()

    parser_example = subparsersgroup.add_parser(
        "example", description="This command does nothing")
    parser_example.add_argument("--verbose", "-v",
                                action='count',
                                help='Also this option does nothing.')
    parser_example.set_defaults(func=example_action)

    parser_sort_by_location = subparsersgroup.add_parser(
        "sort", description="sort parts by location")
    parser_sort_by_location.set_defaults(func=sort_by_location)

    parser_normalize = subparsersgroup.add_parser(
        "normalize", description=("read parts list and write it"
                                  + " so that further automatic"
                                  + " alterations make smaller diffs"))
    parser_normalize.set_defaults(func=normalize_parts_list)

    parser_printed = subparsersgroup.add_parser(
        'printed', description=('Add the "printed" attribute'
                                + ' to all parts so that they'
                                + ' are not printed next time.')
    )
    parser_printed.set_defaults(func=add_printed)

    parser_shortcutreplacement = subparsersgroup.add_parser(
        'shortcut', description=("Replace shortcut 's': "
                                 + "'width height' by "
                                 + "appropriate values."))
    parser_shortcutreplacement.set_defaults(func=convert_shortcuts)

    parser_unprinted = subparsersgroup.add_parser(
        'unprinted', description=('Remove the "printed" attribute'
                                  + " from all parts.")
    )
    parser_unprinted.set_defaults(func=remove_printed)

    parser_rotate = subparsersgroup.add_parser(
        'rotate', description=('Take all images from image directory'
                               + ' and put a landscape version of it'
                               + ' into a directory images_landscape'
                               + ' and a portrait version into'
                               + ' images_portrait. Images that are'
                               + ' almost squares are not rotated.')
    )
    parser_rotate.set_defaults(func=rotate_images)

    return parser.parse_args()


def example_action(args):
    """Does nothing.

    Is an example for a function that is called via a subparser.
    """
    LOGGER.debug('Example action: counted: {}'.format(args.verbose))


def normalize_parts_list(args):
    """Read parts list and write it again."""
    parts = get_parts(args)
    save_parts(parts, args.outputfile)


def sort_by_location(args):
    """Sort a parts list by location."""
    parts = get_parts(args)
    parts.sort(key=(lambda part: part.location.to_sortable_tuple()))
    save_parts(parts, args.outputfile)


def add_printed(args):
    """Add to all parts the attribute printed=True."""
    add_attribute_if(args, lambda part: ("printed", True))


def remove_printed(args):
    """Remove the attribute printed on all parts."""

    def remove(part):
        try:
            del part.printed
        except AttributeError:
            pass

    transform_all_parts(args, remove)


def convert_shortcuts(args):
    """Convert the shortcuts to correct fields:

    "s": "width height"
    "s": "sq" -> square (bigger) cardboard box: 18 x 10
    "s": "sqtp" -> square Tetrapak: 6.5 x 6.5
    "s": "rectp" -> rectangular Tetrapak: 8.5 x 5.5
    """

    def replace_shortcut(part):
        SHORTCUTS = {"sq": (18, 10),
                     "sqtp": (6.5, 6.5),
                     "rectp": (8.5, 5.5)
                     }
        try:
            value = part.s
        except AttributeError:
            return  # no change
        if value in SHORTCUTS:
            part.sign_width = SHORTCUTS[value][0]
            part.sign_height = SHORTCUTS[value][1]
            del part.s
            return
        try:
            width, height = part.s.split()
        except AttributeError:
            LOGGER.warning(
                ("Part {} has attribute 's' "
                 + "but its not a string.").format(part.name))
            return
        except ValueError:
            LOGGER.warning(
                ("Part {} has attribute 's' but its not "
                 + "exactly two items seperated by space.").format(part.name))
            return
        try:
            width = int(width)
            if width <= 0:
                raise ValueError()
        except ValueError:
            try:
                width = float(width)
                if width <= 0:
                    raise ValueError()
            except ValueError:
                LOGGER.warning("width {} in part {} is not a positive number".format(
                    width, part.name
                ))
                return
        try:
            height = int(height)
            if height <= 0:
                raise ValueError()
        except ValueError:
            try:
                height = float(height)
                if height <= 0:
                    raise ValueError()
            except ValueError:
                LOGGER.warning("height {} in part {} is not a positive number".format(
                    height, part.name
                ))
                return
        part.sign_width = width
        part.sign_height = height
        del part.s
        assert hasattr(part, "sign_width")
        assert hasattr(part, "sign_height")
        assert not hasattr(part, "s")
        return

    transform_all_parts(args, replace_shortcut)


def save_parts(parts, path):
    """Save list of parts to file `path`."""
    with open(path, 'w') as outputfile:
        list_to_be_jsoned = [part.to_saveable_dict() for part in parts]
        outputfile.write(json.dumps(
            list_to_be_jsoned, indent=2,
            ensure_ascii=False))


def add_attribute_if(args, attribute, condition=lambda part: True):
    """Add an attribute to all parts that fulfill the condition.

    Attributes:
        args: command line options including in & output file
        attribute:
            function part -> to be added attribute
            as (key, value) tuple
        condition:
            function part -> bool: only add attribute if
            condition(part) is True
    """

    def add_if(part):
        if condition(part):
            key, value = attribute(part)
            setattr(part, key, value)

    transform_all_parts(args, add_if)


def transform_all_parts(args, transformation):
    """Do something with all parts: transformation."""
    parts = get_parts(args)
    for part in parts:
        transformation(part)
    save_parts(parts, args.outputfile)


def loggingconfig(args):
    """Set the basic logging config."""
    try:
        os.mkdir(args.output_dir)
    except FileExistsError:
        pass  # everything fine
    logging.basicConfig(
        filename=os.path.join(args.output_dir, args.logfile),
        level=logging.DEBUG,
        filemode='w'
    )


def rotate_images(args):
    """Sort images into portrait, landscape, rotate where necessary."""
    image_types = ["jpg", "png", "jpeg", "PNG", "JPG", "JPEG"]
    images = [image for image in os.listdir(args.images) if
              os.path.isfile(os.path.join(args.images, image)) and
              os.path.splitext(image)[1][1:] in image_types]
    LOGGER.debug('found files: {}'.format(images))
    for image in images:
        width, height = imagesize.get(
            os.path.join(
                args.images, image)
        )
        if width == -1 or height == -1:
            LOGGER.warning(
                "Image {} seems to be malformed. {}".format(
                    image, "No width and height could be found.")
            )
        elif width >= height:
            # is landscape or square
            if not os.path.isfile(os.path.join(args.images_portrait, image)):
                subprocess.run(['convert', '-rotate', '90',
                                os.path.join(args.images, image),
                                os.path.join(args.images_portrait,
                                             image)])
            if not os.path.isfile(os.path.join(args.images_landscape, image)):
                os.rename(os.path.join(args.images, image),
                          os.path.join(args.images_landscape, image))
        elif width < height:
            # is portrait
            if not os.path.isfile(os.path.join(args.images_landscape, image)):
                subprocess.run(['convert', '-rotate', '90',
                                os.path.join(args.images, image),
                                os.path.join(args.images_landscape, image)])
            if not os.path.isfile(os.path.join(args.images_portrait, image)):
                os.rename(os.path.join(args.images, image),
                          os.path.join(args.images_portrait,
                                       image))


if __name__ == '__main__':
    arguments = parse_args()
    loggingconfig(arguments)
    arguments.func(arguments)
