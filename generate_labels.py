#!/usr/bin/env python3
"""Create a list of bike parts to hang in the bike shed."""
import json
import logging
# import pypandoc if --list option used, see parseArgs
import argparse
import os.path
from typing import List
import treelib

from part import Part
from signprinter import SignPrinter


def check_filename_types(filetypes):
    """Create function that checks for being a simple file.

    If it isn't of one of the specified file types, add the fileending."""

    def check_if_simple_file_name(path):
        """Throw an error if path is not a simple file name."""
        basedir, _ = os.path.split(path)
        if len(basedir) > 0:
            raise argparse.ArgumentTypeError("Only simple file name, not path allowed.")
        _, ext = os.path.splitext(path)
        if ext not in filetypes:
            path += filetypes[0]
        return path

    return check_if_simple_file_name


def add_file_args(parser):
    """Add arguments for file names for parser."""
    parser.add_argument("--input_file", '-in',
                        default='parts.json',
                        help='Input json file with the parts.',
                        type=check_filename_types(['.json'])
                        )
    parser.add_argument('--output_dir', '-d',
                        default='out',
                        help=('Directory where to put the output '
                              + 'files like pdf list, sign html pages')
                        )
    parser.add_argument('--output_mdlist', '-md',
                        default='parts.md',
                        help=('File where to write the list of things'
                              + ' as a markdown file. Relative to out '
                              + 'directory.'),
                        type=check_filename_types(['.md'])
                        )
    parser.add_argument('--output_tree', '-tree',
                        default='parts-tree.txt',
                        help=('File where to write the list of things'
                              + ' as a tree in a text file. Relative '
                              + 'to out directory.'),
                        type=check_filename_types(['.txt']))
    parser.add_argument('--output_pdflist', '-pdf',
                        default='parts.pdf',
                        help=('File where to write the list of things'
                              + ' as a pdf file. Relative to out '
                              + 'directory.'),
                        type=check_filename_types(['.pdf'])
                        )
    parser.add_argument('--output_signs', '-s',
                        default='signs.html',
                        help=('File where to write the html page'
                              + ' with big signs,'
                              + ' relative to out directory.'),
                        type=check_filename_types(['.html', '.htm'])
                        )
    parser.add_argument('--images', '-i',
                        default='images',
                        help=('Directoryprefix where to find the images'
                              + ' referenced by the json parts'
                              + ' input file.')
                        )
    parser.add_argument('--images-landscape', '-il',
                        default="images_landscape",
                        help=('Directoryprefix where to find the images'
                              + ' referenced by the json parts'
                              + ' input file when the landscape version is needed.'
                              + ' Not used yet.'))
    parser.add_argument('--images-portrait', '-ip',
                        default="images_portrait",
                        help=('Directoryprefix where to find the images'
                              + ' referenced by the json parts'
                              + ' input file when the portrait version is needed.'
                              + ' Not used yet.'))
    parser.add_argument('--logfile', '-lf',
                        default='parts.log',
                        help=('Where to write the log file.'
                              + ' Relative to the out directory.'),
                        type=check_filename_types(['.log'])
                        )


def parse_args():
    """Parse the command-line arguments.

    Supply information from command-line arguments.

    Returns:
        Options as an dict-like object.

    """
    parser = argparse.ArgumentParser(
        description="""Create lists and signs for bike parts.

        The output directory is created. If the outputfiles are
        not directly in the output directory, the directories
        where the output files are supposed to land must exist.

        Output files overwrite existing files.
        """
    )
    parser.add_argument(
        '-l', '--list',
        dest='list',
        action='store_true',
        help=('Create list using pandoc.'
              + ' Attention: needs LaTeX installation.')
    )
    add_file_args(parser)
    options = parser.parse_args()

    return options


def import_jsonfile(path):
    """Import data from json file.

    Args:
        path (string): path to the imported file
    Returns:
        dict or list: object with json data
    Raises:
        FileNotFoundError: if path is not a path to a openable file
        json.decoder.JSONDecodeError: if json is malformed

    """
    with open(path) as jsonfile:
        try:
            return json.load(jsonfile)
        except json.decoder.JSONDecodeError as exc:
            logging.error("json file {} is not well-formatted".format(path))
            print("json file", path, "is not well-formatted:")
            raise exc


def get_parts(paths):
    """Get list of parts from json file.

    path: paths for json file and where images are (as from pars args)
    """
    parts = import_jsonfile(paths.input_file)
    parts = [Part(jsonpart, paths) for jsonpart in parts]
    return parts


def filter_proper_parts(parts: List[Part]) -> List[Part]:
    """Return a filtered parts list without categories.

    Categories are parts with the key - value pair "category": true."""
    return [part for part in parts if not (hasattr(part, "category")
                                           and part.category == True)]


def get_markdownlist(parts):
    """Get markdown representation of parts list."""
    # each item can appear several times since alternative names
    # might not be unique: things can be similar.
    # so for every thing we need a list of lines
    lines = {}

    def add_line(name, line):
        if name in lines:
            lines[name].append(line)
        else:
            lines[name] = [line]

    for part in parts:
        add_line(part.name, part.markdown_representation())
        for alt_name, line in part.alt_names_markdown().items():
            add_line(alt_name, line)
    markdown = "\n".join([
        line for name in sorted(list(lines.keys()), key=str.lower)
        for line in lines[name]])
    return markdown


def get_tree(parts: List[Part]) -> str:
    """Create a simple tree structure visualizing the parts."""
    tree = treelib.Tree()
    nodes = []
    tree.create_node('Fahrrad', 'fahrrad-0')
    for part in parts:
        name = part.display_name[0]
        try:
            parents = part.part_of
        except AttributeError:
            parents = ['fahrrad']
        else:
            if isinstance(parents, str):
                parents = [parents]
            assert hasattr(parents, '__iter__')  # not perfect check for parents being an iterable
        for instance_nr, parent in enumerate(parents):
            parent = parent.lower().replace(" ", "_")
            nodeid = name.lower().replace(" ", "_") + "-" + str(instance_nr)
            parent = parent + "-0"  # doesn't work well with categories that appear several times, whatever
            nodes.append((name, nodeid, parent))
    delayed_nodes = []
    inserted_node = True  # save if in previous run,
    # some node got removed from the list of all nodes
    while inserted_node:
        logging.debug("Start insertion walkthrough")
        inserted_node = False
        for node in nodes:
            try:
                tree.create_node(node[0], node[1], node[2])
            except treelib.exceptions.NodeIDAbsentError:
                delayed_nodes.append(node)
            except treelib.exceptions.DuplicatedNodeIdError as e:
                logging.error(f"{node[1]} twice: {e}")
                inserted_node = True
            else:
                inserted_node = True
        nodes = delayed_nodes
        delayed_nodes = []
    if len(nodes) > 0:
        logging.error("Remaining nodes:" + ", ".join((str(node) for node in nodes)))
    return tree.show(stdout=False)


def create_listpdf(parts, pdffile):
    """Create a pdf file that can be printed that lists all parts in parts."""
    markdown = get_markdownlist(parts)
    pypandoc.convert_text(markdown, format='md', to='pdf',
                          outputfile=pdffile,
                          extra_args=["-V", "geometry:top=1cm,bottom=1cm,left=1cm,right=1.5cm",
                                      "-V", "classoption=twocolumn"])


if __name__ == '__main__':
    # filemode = 'w' -> overwrite
    OPTIONS = parse_args()
    try:
        os.mkdir(OPTIONS.output_dir)
    except FileExistsError:
        pass  # everything fine
    logging.basicConfig(filename=os.path.join(OPTIONS.output_dir, OPTIONS.logfile),
                        level=logging.DEBUG,
                        filemode='w'
                        )
    logging.info("Create parts list")
    ALLPARTS = get_parts(OPTIONS)
    with open(os.path.join(
            OPTIONS.output_dir, OPTIONS.output_tree),
            mode="w") as treefile:
        logging.info("Start creating tree view.")
        treefile.write(get_tree(ALLPARTS))
    ALLPARTS = filter_proper_parts(ALLPARTS)
    logging.info("Start creating markdownlist.")
    with open(os.path.join(
            OPTIONS.output_dir, OPTIONS.output_mdlist),
            mode='w') as mdfile:
        mdfile.write(get_markdownlist(ALLPARTS))
    if OPTIONS.list:
        logging.info("Start creating list pdf.")
        # import is down here to not impose the need of LaTeX (via pandoc) to all users
        # but just to those who choose it
        import pypandoc

        create_listpdf(ALLPARTS, os.path.join(
            OPTIONS.output_dir, OPTIONS.output_pdflist))
    logging.info("Start creating sign html.")
    PRINTER = SignPrinter(OPTIONS)
    PRINTER.save_signs_html(ALLPARTS)
