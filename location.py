"""Location in the shelf."""
import collections
import logging

"""None means the entire thing one orderlevel higher.

Valid values additional to None are:
    shelf_part: "left", "center", "right", "leftcenter"
    level: int >= 1 (<= 16 is reasonable)
    number_in_level: int >= 1 (<= 8 is reasonable)
    depth: int >= 1 (<= 3 is reasonable)
    height: int >= 1 (<= 2 is reasonable)
"""
SimpleLocation = collections.namedtuple("SimpleLocation",
                                        ["shelf_part",
                                         "level",
                                         "number_in_level",
                                         "depth",
                                         "height"],
                                        defaults=[None]*5)

class Location(SimpleLocation):
    """A location in the bike shelf. Member vars explained above."""
    def same_box(self, other):
        """Return if this and other are in the same box.

        That is true if self and other only differ in depth or height.
        """
        return (self.shelf_part == other.shelf_part
                and self.level == other.level
                and self.number_in_level == other.number_in_level)

    def truncate_to_box(self):
        """Return the location of the surrounding marked box.

        When labelling we label boxes with different parts in it.
        So this method gives the box to a location.

        It does this by setting depth and height to None.
        """
        return Location(self.shelf_part, self.level, self.number_in_level)

    def __str__(self):
        """Get string representation of the location."""
        tostring = "{}".format({
            "side": "Side",
            "left": "L",
            "center": "M",
            "right": "R",
            "leftcenter": "L/M",
            "top-a": "A",
            "top-b": "B",
            None: "?"}[self.shelf_part])
        if self.shelf_part is None or self.level is None:
            return tostring
        tostring += str(self.level)
        if self.number_in_level is None:
            return tostring
        tostring += "-{}".format(self.number_in_level)
        if self.depth is not None:
            try:
                tostring += " abcde"[self.depth]
                # depth = 0 does not exist, start at 1 = a
            except TypeError as error:
                logging.warning(
                    "Type Error at reading "
                    + "depth ({}): {}".format(
                        self.depth,
                        error
                    )
                    + "Take depth itself instead."
                    + "Location parsed so far: "
                    + tostring)
                tostring += self.depth
        if self.height is not None:
            tostring += ["", "I", "II", "III", "IV"][self.height]
        return tostring

    def to_sortable_tuple(self):
        """Return the data of this location a sortable tuple.

        shelf_part: None < A < B < Side < left < center < leftcenter < right
        level: int, null = 1
        number_in_level: int, null = 1,
        depth: int, null = 1,
        height: int, null = 1
        """
        sorting_degrees = [None]*5
        try:
            sorting_degrees[0] = {
                None: 0,
                "top-a": 1,
                "top-b": 2,
                "side": 3,
                "left": 4,
                "center": 5,
                "leftcenter": 6,
                "right": 7
            }[self.shelf_part]
        except KeyError:
            logging.warning(
                "The location {} has an unsupported shelf part.".format(self))
            sorting_degrees[0] = 0
        index = 1
        for number in [
            self.level,
            self.number_in_level,
            self.depth,
            self.height
            ]:
            if number is None:
                sorting_degrees[index] = 1
            else:
                sorting_degrees[index] = number
            index += 1
        return tuple(sorting_degrees)

