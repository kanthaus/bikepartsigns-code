#!/usr/bin/env python3
"""Class representing a bike part (category)."""
import os.path
import logging
import slugify
from location import Location


class Part:
    """Represents one part of a bike.

    Supplies functions to represent this bike part.

    All values from an json import are available as object members
    (e.g. part.width = 3.0 if "width": 3.0 is a line in json.
    """

    # all lenghts in cm
    STANDARD_WIDTH = 8
    STANDARD_HEIGHT = 8
    MAX_WIDTH = 18 # otherwise too wide for A4 page
    MAX_HEIGHT = 28 # otherwise too long for A4 page

    def __init__(self, json, image_paths):
        """Create new part based on json file content for one part.

        Arguments:
            json: dict read from json file with part data
            image_paths: object with image paths:
                images-landscape
                images
                images-portrait"""
        for key in json:
            setattr(self, key, json[key])
        try:
            self.name = self.name_de
        except AttributeError:
            try:
                self.name = self.name_en
            except AttributeError:
                self.name = "?"
        self.logger = logging.getLogger(__name__)
        self._extract_location()
        self._where = str(self.location)
        self._extract_image(image_paths)


    @property
    def where(self):
        """Get where the part is as a printable string."""
        return self._where

    @property
    def location(self):
        """Get where the part is as a Location object."""
        return self._location

    @property
    def sign_size(self):
        """Get (sign width, height), use default if not given."""
        try:
            sign_width = self.sign_width
        except AttributeError:
            sign_width = self.STANDARD_WIDTH
        try:
            sign_height = self.sign_height
        except AttributeError:
            sign_height = self.STANDARD_HEIGHT
        return (min(sign_width, self.MAX_WIDTH),
                min(sign_height, self.MAX_HEIGHT))

    @property
    def display_name(self):
        """Get the (german, english) name that shall be printed on the sign."""
        try:
            german = self.name_sign_de if hasattr(self, "name_sign_de") else self.name_de
        except AttributeError:
            german = ""
            self.logger.warning("German name for {} missing for printing.".format(
                self.name))
        try:
            english = self.name_sign_en if hasattr(self, "name_sign_en") else self.name_en
        except AttributeError:
            english = ""
            self.logger.warning("English name for {} missing for printing".format(
                self.name))
        return german, english

    def _extract_location(self):
        """Set member location and where to a the locality of the part.

        self.location will be a Location object.
        self.where will be a string.


        """
        try:
            shelf_part = self.shelf_part
        except AttributeError:
            self._location = Location()
            return
        try:
            level = self.level
        except AttributeError:
            self._location = Location(shelf_part)
            return
        try:
            number_in_level = self.number_in_level
        except AttributeError:
            self._location = Location(shelf_part, level)
            return
        try:
            depth = self.depth
        except AttributeError:
            depth = None
        try:
            height = self.height
        except AttributeError:
            height = None
        self._location = Location(shelf_part, level, number_in_level,
                                  depth, height)

    def _extract_image(self, image_dirs):
        """If no image is set, guess a file name and set it if it exists.

        Arguments:
            image_dirs:
                objects with member variables with images dirs
        """
        if hasattr(self, "image"):
            for directory in [image_dirs.images_landscape,
                              image_dirs.images_portrait]:
                image_path = os.path.join(directory, self.image)  # pylint: disable=access-member-before-definition
                if not os.path.isfile(image_path):
                    self.logger.warning("Image `{}` does not exist.".format(
                        image_path))
            return
        normalizedname = slugify.slugify(self.name, separator="_")
        for filetype in ["jpg", "png", "jpeg", "PNG", "JPG", "JPEG"]:
            for directory, other_directory in [
                (image_dirs.images_landscape, image_dirs.images_portrait),
                (image_dirs.images_portrait, image_dirs.images_landscape)]:
                image = "{}.{}".format(normalizedname, filetype)
                imagepath = os.path.join(directory, image)
                if os.path.isfile(imagepath):
                    self.logger.info(
                        'Use guessed image name: {image} for {thing}'.format(
                            image=image, thing=self.name))
                    self.image = image
                    if not os.path.isfile(
                        os.path.join(other_directory,
                                     image)):
                        self.logger.warning(
                            ('Guessed image name {image} for '
                             + '{thing} does not exist in '
                             + 'directory {dir}').format(
                                 image=image,
                                 thing=self.name,
                                 dir=other_directory)
                        )
                    return  # do not try other filetypes or other dir

    def markdown_representation(self):
        """Create representation in markdown syntax for this part."""
        title = "- **{}**".format(self.name)
        if hasattr(self, "name_de"):
            # german name saved in self.name, add english name
            try:
                title += " (**{}**)".format(self.name_en)
            except AttributeError:
                pass
        markdown = "{}: {}".format(title, self.where)
        return markdown

    def alt_names_markdown(self):
        """Create markdown lines with references of alternative names.

        Each line is a dictionary entry mapping the alt name to the
        markdown line.
        """
        alternative_names = []
        try:
            alternative_names = self.name_alt_de
        except AttributeError:
            pass
        try:
            alternative_names.extend(self.name_alt_en)
        except AttributeError:
            pass
        # if there is a german name the english name is only in
        # paranthesis behind the german name and therefore not
        # in the correct alphabetical place
        # so add it to alternative names
        try:
            if hasattr(self, "name_de"):
                alternative_names.append(self.name_en)
        except AttributeError:
            pass # no english name, no need for redirecting
        markdownlines = {alternative_name : "- **{}** → {} {}".format(
            alternative_name, self.name, self.where)
                         for alternative_name in alternative_names}
        return markdownlines

    def to_saveable_dict(self):
        """Convert to dict that can be saved and converted back to the same part.

        Should not have any doubled (inferred) information.
        """
        # when writing the resulting dict to
        # json file the following order is used:
        # (modern python preserves order for dicts by default!)
        keys = ['name_de',
                'name_sign_de',
                'fontsize_sign_de',
                'name_alt_de',
                'name_en',
                'name_sign_en',
                'fontsize_sign_en',
                'name_alt_en',
                'shelf_part',
                'level',
                'number_in_level',
                'depth',
                'height',
                'image',
                'sign_width',
                'sign_height',
                'printed']
        not_saved_keys = ['_where', 'name', '_location', 'logger']
        keys.extend([key for key in vars(self).keys() if
                     key not in not_saved_keys])
        return {key : getattr(self, key) for key in keys
                if hasattr(self, key)}
