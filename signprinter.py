#!/usr/bin/env python3
"""Create a printable webpage for bike part signs."""
import os.path
import logging

TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), "signtemplate")
TEMPLATE_MAIN = "template.html"
TEMPLATE_SIGN = "sign.html"
TEMPLATE_SIGN_LANDSCAPE = "sign_landscape.html"
TEMPLATE_CSS_HEADER = "header.css"
TEMPLATE_PAPER_CSS = "paper.css"

UNIT = "cm"
# estimated by one example word
# at standard fontsize german
STANDARD_LETTER_LENGTH = 0.55
STANDARD_FONTSIZE_GERMAN = 1
STANDARD_FONTSIZE_ENGLISH = 0.8
# in symbols:
STANDARD_LINE_LENGTH = 15
# how much bigger german should roughly be than english
GERMAN_TO_ENGLISH_SHARE = 1.25
# how big the image should be in parts of entire sign (in portrait mode)
IMAGE_SHARE = 0.55
# how much of the sign height can actually be used for text (due to margins)
# (in landscape mode)
TEXT_SHARE = 0.7
# minimum ration width/height for using landscape mode
LANDSCAPE_MIN_RATIO = 2

class SignPrinter:
    """Class encapsulating algorithm for creating printable signs from parts list.

    Bad design choice: you have to create an object but actually this object is never used,
    could all just be module or class functions.
    """

    def __init__(self, paths):
        """Create SignPrinter by reading in template files."""
        # pylint: disable=unbalanced-tuple-unpacking
        (self.main, self.signhtml, self.signhtml_landscape,
         self.css, self.papercss) = self._read_templates()
        self.image_dir_landscape = paths.images_landscape
        self.image_dir_portrait = paths.images_portrait
        self.output_dir = paths.output_dir
        self.signs_file = paths.output_signs
        self.logger = logging.getLogger(__name__)

    def _read_templates(self):
        """Read the templates from the files.

        Returns:
            (iterable) main template, css header,
            single size sign html file, double size sign html file
        """
        filecontents = []
        for filename in [TEMPLATE_MAIN, TEMPLATE_SIGN, TEMPLATE_SIGN_LANDSCAPE,
                         TEMPLATE_CSS_HEADER, TEMPLATE_PAPER_CSS]:
            with open(os.path.join(TEMPLATE_DIR, filename)) as file:
                filecontents.append(file.read())
        return filecontents

    def _sort_parts_by_location(self, parts):
        """Sort parts into dictionary mapping locations to list of parts.

        All parts in the list mapped to by one location are in the same
        location. Same as in location.Location.sameBox()

        Unused.
        """
        location_to_parts = {}
        for part in parts:
            if part.location.truncate_to_box() in location_to_parts:
                location_to_parts[part.location.truncate_to_box()].append(
                    part)
            else:
                location_to_parts[part.location.truncate_to_box()] = [part]
        return location_to_parts

    def if_use_landscape_template(self, part):
        """Return if we want to use the landscape template for this part."""
        try:
            return part.sign_landscape
        except AttributeError:
            width, height = part.sign_size
            return width >= height * LANDSCAPE_MIN_RATIO

    def guess_fontsize(self, part):
        """Guess good fontsize.

        Based on the length of the name and the size of the sign.

        Returns:
            guessed fontsize german, guessed fontsize english in UNIT
        """
        if self.if_use_landscape_template(part):
            return self.guess_fontsize_landscape(part)
        return self.guess_fontsize_portrait(part)

    def guess_fontsize_landscape(self, part):
        """Guess good fontsizes for the landscape template."""
        # for simplicity assume only one line
        width, height = part.sign_size
        used_width = width - height  # that is approximately the part the image uses
        german, english = part.display_name
        german_expected_width_at_standard = len(german) * STANDARD_LETTER_LENGTH
        german_max_by_width = (STANDARD_FONTSIZE_GERMAN * used_width
                                / german_expected_width_at_standard)
        german_max_by_height = (
            height * TEXT_SHARE / (GERMAN_TO_ENGLISH_SHARE + 1)
            * GERMAN_TO_ENGLISH_SHARE)
        english_expected_width_at_standard = (
            len(english) * STANDARD_LETTER_LENGTH
            * STANDARD_FONTSIZE_ENGLISH / STANDARD_FONTSIZE_GERMAN)
        english_max_by_width = (STANDARD_FONTSIZE_ENGLISH * used_width
                                / english_expected_width_at_standard)
        english_max_by_height = (
            height * TEXT_SHARE / (GERMAN_TO_ENGLISH_SHARE + 1))
        return (min(german_max_by_width, german_max_by_height),
                min(english_max_by_width, english_max_by_height))

    def guess_fontsize_portrait(self, part):
        """Guess what a good fontsize is for this sign.

        Based on the length of the name and the size of the sign.

        Returns:
            guessed fontsize german, guessed fontsize english in UNIT
        """
        german, english = part.display_name
        german_words = german.replace('-', "- ").split()
        english_words = english.replace('-', " ").split()
        # ignore cases with more than 2 lines, should be considered by hand
        max_fontsizes = [[0, 0, 0]]*4
        GERMAN_INDEX = 0
        ENGLISH_INDEX = 1
        SUM_INDEX = 2
        # self.logger.debug("g: {}; e: {}, width: {}, heigth: {}".format(
        #     german, english, *part.sign_size  # unpacking pair
        # ))
        width, height = part.sign_size
        for german_number_lines, english_number_lines, case in [
                (1, 1, 0), (1, 2, 1), (2, 1, 2), (2, 2, 3)]:
            german_length = (max([len(word) for word in german_words])
                             if german_number_lines == 2 else len(german))
            english_length = (max([len(word) for word in english_words])
                              if english_number_lines == 2 else len(english))
            german_expected_width_at_standard = german_length * STANDARD_LETTER_LENGTH
            german_max_by_width = (STANDARD_FONTSIZE_GERMAN * width
                                   / german_expected_width_at_standard)
            german_max_by_height = (height * (1 - IMAGE_SHARE)  # text portion
                                    / (german_number_lines * GERMAN_TO_ENGLISH_SHARE
                                       + english_number_lines)
                                    * GERMAN_TO_ENGLISH_SHARE)
            english_expected_width_at_standard = (
                english_length * STANDARD_LETTER_LENGTH
                * STANDARD_FONTSIZE_ENGLISH / STANDARD_FONTSIZE_GERMAN)
            english_max_by_width = (STANDARD_FONTSIZE_ENGLISH * width
                                    / english_expected_width_at_standard)
            english_max_by_height = (height * (1 - IMAGE_SHARE)  # text portion
                                     / (german_number_lines * GERMAN_TO_ENGLISH_SHARE
                                        + english_number_lines))
            max_fontsizes[case][GERMAN_INDEX] = min([german_max_by_height, german_max_by_width])
            max_fontsizes[case][ENGLISH_INDEX] = min([english_max_by_height, english_max_by_width])
            max_fontsizes[case][SUM_INDEX] = (max_fontsizes[case][GERMAN_INDEX]
                                              + max_fontsizes[case][ENGLISH_INDEX])
            # self.logger.debug(
            #     "case: {}; gmaxH: {:.3f}; gmaxW: {:.3f}; emaxH: {:.3f}; emaxW: {:.3f}".format(
            #         case, german_max_by_height, german_max_by_width,
            #         english_max_by_height, english_max_by_width
            #     )
            # )
        german_max, english_max, _ = max(max_fontsizes, key=(lambda case: case[SUM_INDEX]))
        # self.logger.debug("used fs: g: {:.3f}, e: {:.3f}".format(german_max, english_max))
        return german_max, english_max

    def get_fontsize(self, part):
        """Determine font size of sign for part.

        Take font size in the part.
        Guess font size if not specified.

        Returns:
            (german font size, english font size)
        """
        try:
            german_font_size = part.fontsize_sign_de
        except AttributeError:
            german_font_size = None
        try:
            english_font_size = part.fontsize_sign_en
        except AttributeError:
            english_font_size = None
        if german_font_size is None and english_font_size is None:
            german_font_size, english_font_size = self.guess_fontsize(part)
        elif german_font_size is None:
            # guessing guesses both together, not build for single guess
            # if you start to wriggle by hand with the font sizes, just do both
            german_font_size = STANDARD_FONTSIZE_GERMAN
        elif english_font_size is None:
            english_font_size = STANDARD_FONTSIZE_ENGLISH
        return german_font_size, english_font_size

    def create_html(self, parts):
        """Create html (as str) that shows all signs.

        Arguments:
            parts: list of parts to be described
            paths: object with member variables images, output_dir (where paper.css file is in)

        """
        content_html = []
        for part in parts:
            content_html.append(self.create_sign(part))
        # type change of variable!:
        content_html = "\n".join(content_html)
        return self.main.format(signs=content_html, css_stuff=self.css)

    def get_values_for_template(self, part):
        """Get values for the insertion into the templates.

        Only the values that are common for portrait
        and landscape template:
            german
            english
            image
            width and height css-string
            fontsize css-string (switch inside finding fontsize methods)
        """
        insertions = {}

        insertions["german"], insertions["english"] = part.display_name

        if self.if_use_landscape_template(part):
            rel_path_to_image_dir = os.path.relpath(self.image_dir_portrait, self.output_dir)
        else:
            rel_path_to_image_dir = os.path.relpath(self.image_dir_landscape, self.output_dir)
        try:
            insertions["image"] = os.path.join(rel_path_to_image_dir, part.image)
        except AttributeError:
            insertions["image"] = ""

        insertions["location"] = str(part.location)

        width, height = part.sign_size
        insertions["style-sign"] = "width: {}{}; height: {}{}".format(width, UNIT, height, UNIT)

        fontsizes = self.get_fontsize(part)
        insertions["style-german"] = "font-size: {}{};".format(fontsizes[0], UNIT)
        insertions["style-english"] = "font-size: {}{};".format(fontsizes[1], UNIT)
        return insertions

    def create_sign(self, part):
        """Create a sign based on the template sign.html."""
        # text that is to be inserted into the template
        insertions = self.get_values_for_template(part)
        if self.if_use_landscape_template(part):
            return self.signhtml_landscape.format(
                **insertions # unpacking dictionary
                )
        return self.signhtml.format(
            **insertions  # unpacking dictionary
            )

    def separte_parts_by_sign_size(self, parts):
        """Sort parts by the sign size they need.

        Deprecated right now.

        If a part has the information saved itself,
        use that.

        Other parts depend on whether they are alone
        in their box or not. Alone -> big sign,
        together -> small sign.

        Returns:
            tuple of two lists of parts
        """
        double_sign_parts = []
        single_sign_parts = []
        for part in parts:
            if hasattr(part, 'sign_size'):
                if part.sign_size == "single":
                    single_sign_parts.append(part)
                elif part.sign_size == "double":
                    double_sign_parts.append(part)
        location_parts_lists = self._sort_parts_by_location(parts)
        for location in location_parts_lists:
            if len(location_parts_lists[location]) == 1:
                for part in location_parts_lists[location]:
                    # avoid duplicates
                    if (part not in double_sign_parts
                            and part not in single_sign_parts):
                        double_sign_parts.append(part)
            else:
                for part in location_parts_lists[location]:
                    if (part not in double_sign_parts
                            and part not in single_sign_parts):
                        single_sign_parts.append(part)
        assert set(double_sign_parts).union(set(single_sign_parts)) == set(parts), (
            "Not all parts were sorted.")
        assert len(double_sign_parts) + len(single_sign_parts) == len(parts), (
            "Not all parts are sorted uniquely.")
        return single_sign_parts, double_sign_parts

    def save_signs_html(self, parts):
        """Save signs as html to file path. Saves necessary css next to it.

        Ignore parts that should not be printed as saved in part.printed.

        Arguments:
            paths (dict):
                object with file paths

        """
        parts.sort(key=(lambda part: part.location.to_sortable_tuple()))
        parts = [part for part in parts if not (hasattr(part, 'printed') and part.printed)]
        # single_sign_parts, double_sign_parts = self.separte_parts_by_sign_size(parts)
        file_signs = os.path.join(self.output_dir, self.signs_file)
        with open(file_signs, mode="w") as html_file:
            html_file.write(self.create_html(parts))
        # the next two items are just copying
        # probably more efficiant with shutils but that would be one more dependency
        with open(os.path.join(self.output_dir, TEMPLATE_PAPER_CSS), mode="w") as paper_file:
            paper_file.write(self.papercss)
        with open(os.path.join(self.output_dir, TEMPLATE_CSS_HEADER), mode="w") as header_file:
            header_file.write(self.css)
